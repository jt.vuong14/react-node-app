import React, { Component } from 'react';
import './styles.css';
import './util.css';
import { connect } from 'react-redux';
import bg_image from './assets/bg-01.jpg';
import gg_icon from './assets/icon-google.png';

class SigninPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputEmail: '',
            inputPassword: '',
        };
    }

    onEmailChange = (event) => {
        this.setState({
            inputEmail: event.target.value
        });
    }

    onPasswordChange = (event) => {
        this.setState({
            inputPassword: event.target.value
        });
    }

    handleSignIn = () => {
        this.props.signin(this.state.inputEmail, this.state.inputPassword);
    }

    render() {
        return (
            <div>
            	<div className="limiter">
				<div className="container-login100" style={{backgroundImage: `url(${bg_image})`}}>
				<div className="wrap-login100 p-l-70 p-r-70 p-t-62 p-b-33">
				<div className="login100-form validate-form flex-sb flex-w">
					<span className="login100-form-title p-b-35">
						Đăng nhập
					</span>

					<a href="/auth/facebook" className="btn-face m-b-15">
						<i className="fa fa-facebook-official"></i>
						Facebook
					</a>

					<a href="/auth/google" className="btn-google m-b-15">
						<img src={gg_icon} alt="GOOGLE"/>
						Google
					</a>
					
					<div className="p-t-31 p-b-9">
						<span className="txt1">
							Username
						</span>
					</div>
					<div className="wrap-input100 validate-input" data-validate = "Username is required">
						<input className="input100" type="text" name="username" onChange={this.onEmailChange}/>
						<span className="focus-input100"></span>
					</div>
					
					<div className="p-t-13 p-b-9">
						<span className="txt1">
							Password
						</span>
					</div>
					<div className="wrap-input100 validate-input" data-validate = "Password is required">
						<input className="input100" type="password" name="pass" onChange={this.onPasswordChange}/>
						<span className="focus-input100"></span>
					</div>

					<div className="container-login100-form-btn m-t-17">
						<button className="login100-form-btn" onClick={this.handleSignIn}>
							Đăng nhập
						</button>
					</div>

					<div className="w-full text-center p-t-55">
						<a href="/forgot-password" className="txt2 bo1 m-l-5">
							Quên mật khẩu?
						</a>
						<a href="/signup" className="txt2 bo1">
							Đăng ký
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
    </div>
        );
    }
};

const mapDispatchToProps = {}

export default connect(
    null, mapDispatchToProps
)(SigninPage);




