import axios from 'axios';
import { FETCH_USER_API } from '../constants/apis';

export const fetchUserAPI = async () =>{
    const res = await axios.get(FETCH_USER_API);
    return res.data;
}