import { fetchUserAPI } from '../../apiRequests/fetchUser';
import { fetchUser } from './actions';

export const fetchUserThunk = () => async dispatch =>{
    const response = await fetchUserAPI();
    dispatch(fetchUser(response))
}