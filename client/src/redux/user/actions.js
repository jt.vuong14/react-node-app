import { FETCH_USER } from './types';

export const fetchUser = (data) => async dispatch =>{
    dispatch({ type: FETCH_USER, payload: data  })
}