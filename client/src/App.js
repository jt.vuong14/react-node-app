import React from 'react';
import ConnectedRouter from './router/Router';

const App = () => {
  return (
    <ConnectedRouter />
  );
}

export default App;
