import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Header extends Component {
    renderContent = () => {
        switch (this.props.user){
            case null:
                return;
            case false:
                return (
                    <li><a href='/auth/google'>Login with google</a></li>
                )
            default:
                return (
                    <li><a href='/api/logout'>Logout</a></li>
                )
        }
    }

    render(){
        return (
            <nav>
                <div className="nav-wrapper">
                    <Link 
                        to={this.props.auth ? '/surveys' : '/'}
                        className="left brand-logo"
                    >
                        Header
                    </Link>
                  <ul className="right">
                    {this.renderContent()}
                  </ul>
                </div>
            </nav>
        )
    }
}

function mapStateToProps(state) {
    return { user: state.user };
}
export default connect(mapStateToProps)(Header);