import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const AuthenticationRoute = (props) => {
    const {component, authed, ...rest } = props;
    return (
        <Route
            {...rest}
            render={componentProps => {
                return (
                    props.authed
                        ? <Redirect to={'/'} />
                        : React.createElement(props.component, componentProps)
                );
            }}
        />
    );};

export default AuthenticationRoute;