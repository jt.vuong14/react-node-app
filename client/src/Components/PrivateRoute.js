import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = (props) => {
    const {component, authed, ...rest } = props;
    return (
        <Route
            {...rest}
            render={componentProps => {
                return (
                    props.authed
                        ? React.createElement(props.component, componentProps)
                        : <Redirect to={'/sign-in'} />
                );
            }}
        />
    );};

export default PrivateRoute;