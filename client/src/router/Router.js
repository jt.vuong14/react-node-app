
import React, { Component } from 'react';
import { Router, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import  SigninPage from '../pages/SignIn/SignIn';
import Landing from '../pages/Landing/Landing';
import PrivateRoute from '../Components/PrivateRoute';
import AuthenticationRoute from '../Components/AuthenticationRoute';
import history from '../history/index';
import {fetchUserThunk} from '../redux/user/thunkMiddlewares';
import { APP_URL } from '../constants/appConstants';

class Routes extends Component{
    componentDidMount(){
        this.props.fetchUserThunk();
    }

    render() {
        const isAuthenticated = ( this.props.user !==  null ) && ( this.props.user !== false );
        return (
                <Router history={history}>
                    <AuthenticationRoute exact path={APP_URL.SIGN_IN} component={SigninPage} authed={isAuthenticated} />
                    <Switch>
                        <PrivateRoute exact path={APP_URL.HOME_PAGE} component={Landing} authed={isAuthenticated}/>
                    </Switch>
                </Router>
        )
    }
}
const mapStateToProps = state => ({ user: state.user});
const mapActionsToProps = { fetchUserThunk};
export default connect(mapStateToProps, mapActionsToProps)(Routes);