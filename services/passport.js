const passport = require('passport');
const FaceBookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const keys = require('../config/keys');
const mongoose = require('mongoose');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id).then(user => {
        done(null, user);
    });
});

passport.use(
    new GoogleStrategy(
        {
            clientID: keys.googleClientID,
            clientSecret: keys.googleClientSecret,
            callbackURL: '/auth/google/callback',
            proxy: true
        },
        async (accessToken, refreshToken, profile, done ) => {
            const existingUser = await User.findOne({googleId: profile.id});
            if(existingUser){
                return done(null, existingUser);
            }
            const user = await new User({
                googleId: profile.id,
                name: profile._json.name,
                avatar: profile._json.picture,
                email: profile._json.email
            }).save();
            done(null, user);
        }
    )
);

passport.use(
    new FaceBookStrategy({
        clientID: keys.facebookClientID,
        clientSecret: keys.facebookClientSecret,
        callbackURL: '/auth/facebook/callback',
        proxy: true,
        profileFields: ['id', 'displayName', 'photos', 'email']
    },
    async (accessToken, refreshToken, profile, done) => {
        console.log('facebook', profile._json.picture.data)
        const existingUser = await User.findOne({facebookId: profile.id});
        if(existingUser){
            return done(null, existingUser);
        }
        const user = await new User({
            facebookId: profile.id,
            name: profile._json.name,
            avatar: profile._json.picture.data.url
        }).save();
        done(null, user);
    }
));