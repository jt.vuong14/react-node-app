const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema ({
    googleId: String,
    facebookId: String,
    role: { type: String, default: 'User'},
    name: String,
    avatar: String,
    email: String,
})

mongoose.model('users', userSchema);